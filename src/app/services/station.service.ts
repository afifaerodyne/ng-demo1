import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { getUrl, otObs } from '../common'

@Injectable({
  providedIn: 'root'
})
export class StationService {

  get statsUrl() {
    return getUrl('stations')
  }

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Return list of stations with no filteration nor sorting
   */
  list() {
    return otObs(this.http.get(this.statsUrl))
  }

}
