import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { MapboxComponent } from './mapbox.component'
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  {
    path: '',
    component: MapboxComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapboxRoutingModule {}

@NgModule({
  declarations: [MapboxComponent],
  imports: [
    CommonModule,
    MapboxRoutingModule
  ],
  exports: [MapboxComponent]
})
export class MapboxModule { }
