import { Component, OnInit } from '@angular/core'
import * as mapboxgl from 'mapbox-gl'
import { StationService } from 'src/app/services/station.service'

@Component({
  selector: 'app-mapbox',
  templateUrl: './mapbox.component.html',
  styleUrls: ['./mapbox.component.scss']
})
export class MapboxComponent implements OnInit {

  /** Current map object */
  map: mapboxgl.Map

  constructor(
    private stationServ: StationService
  ) { }

  ngOnInit(): void {
    this.initMap()
    this.getStations()
  }

  /**
   * Initialize mapbox
   */
  initMap() {
    const accessToken = 'pk.eyJ1IjoiYWZpZmFlcm9keW5lIiwiYSI6ImNraDA4MHJ6YjA0NGUyc3BtdGdneWRlcHQifQ.hoCY2IB7Zx5_wuR1zX4YJw'
    this.map = new mapboxgl.Map({
      accessToken,
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
      center: [-74.5, 40], // starting position [lng, lat]
      zoom: 9, // starting zoom
      pitch: 45
    })
  }

  /** Retrieve stations that has been saved in db */
  async getStations() {
    const stats = await this.stationServ.list()

    console.log('stats: ', stats)
  }

  /**
   * Retrieves the current location
   */
  getCurr() {
    if (navigator?.geolocation) {
      const geo = navigator.geolocation

      geo.getCurrentPosition((post) => {
        const latlng = [post?.coords?.latitude, post?.coords?.longitude]

        const loc = new mapboxgl.LngLat(latlng[1], latlng[0])

        console.log('Latlng retrieved: ', latlng)
        this.map.flyTo({
          center: loc
        })
      })
    }
    else
      console.error('Unable to call navigator.geolocation')
  }

}
