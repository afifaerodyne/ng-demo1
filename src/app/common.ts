/** common methods */

import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { environment } from 'src/environments/environment'

/**
 * Transform one-time observables into promise
 * NOTE: Can handle errors here as well
 * @param obs One Time Observables
 */
export const otObs = (obs: Observable<any>) => {
    return obs.pipe(
        map(res => res)
    ).toPromise()
}

/** Wrapper to include the domain and api into the url */
export const getUrl = (path: string, root?: boolean) => {
    if (root)
        return `${environment.domain}/${path}`
    else
        return `${environment.domain}/api/${path}`
}