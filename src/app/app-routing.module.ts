import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'mapbox',
    loadChildren: () => import('./components/mapbox/mapbox.module').then(m => m.MapboxModule)
  },
  {
    path: '**',
    loadChildren: () => import('./components/landing/landing.module').then(m => m.LandingModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
